<h2>A faire</h2>
- [X] Faire un parser de formule depuis un fichier
- [X] Stocker l'automate dans un fichier .dot pour visualiser plus simplement

- [X] [Construction de l'automate naïf](https://en.wikipedia.org/wiki/Linear_temporal_logic_to_B%C3%BCchi_automaton)
  - [X] Mettre formule sous [negation formal form](https://en.wikipedia.org/wiki/Linear_temporal_logic#Negation_normal_form)
  - [X] Construire les états
  - [X] Construire les transitions
  - [X] Debug de la construction
  - [X] Règle de sémantique pour W dans les transitions

- [X] [Optimisation sur les formules](https://en.wikipedia.org/wiki/Linear_temporal_logic#Equivalences)
  - [X] Optimisation des formules du calcul prop
  - [X] Idempotence
  - [X] Absorption
  - [X] Divers simplifications
- [X] Optimisation sur les états
  - [X] Optimisation pour G
  - [X] Suppression d'états non accessibles

- [X] Ajouter les opérateurs du passé

- [X] Rapport
  - [X] Preuve de la construction de l'automate
  - [X] Complexité


<h2>Construire un automate depuis un fichier</h2>
executer 'exec fichier'


<h2>Afficher les options</h2>
executer 'exec -help'


<h2>Fichier .dot</h2>
Le langage [DOT](https://fr.wikipedia.org/wiki/DOT_(langage)) permet de décrire
des graphes facilement. On peut alors les stocker dans un fichier 
d'extension .dot.<br>
Pour lire un fichier .dot, il faut utiliser un utilitaire tel que 
[xdot](https://github.com/jrfonseca/xdot.py)