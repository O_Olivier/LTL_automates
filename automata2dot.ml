(* Module pour stocker un automate dans un fichier .dot *)
open Syntax
open Automata
open Printf

(* Alias *)
let pr = output_string

(* Ouvre un fichier (eventuellement nouveau)
 * d'extension .dot *)
let open_dot_file f_name =
  open_out (sprintf "%s.dot" f_name)

(* Imprime les propriétés d'un états :
 * - initial ou pas
 * - acceptant ou pas
 * - son label *)
let pr_proprietes_etat
      ((q,nom):state) (initial:bool) (acceptant:bool) f_out =
  let prop_atom =
    List.filter est_prop_atomique q in
  pr f_out nom;
  pr f_out (" [label=\"["^nom^"]{");
  pr f_out (formule_liste_to_string prop_atom);
  pr f_out "}\"";
  if initial then pr f_out ", color=green";
  if acceptant then pr f_out ", shape=doublecircle";
  pr f_out "]\n"

(* Imprime les propriétés de tous les états *)
let pr_proprietes_etats
      (st_list:state list) etats_init etats_acceptant f_out =
  List.iter
    (fun s ->
      let initial = List.mem s etats_init in
      let acceptant = List.mem s etats_acceptant in
      pr_proprietes_etat s initial acceptant f_out)
    st_list

(* Filtre les transitions de l'automate pour
 * ne garder que celle qui partent de l'état
 * passé en argument *)
let transitions_depuis_etat ((_,nom):state) (tr:transitions) =
  List.filter
    (fun ((_,nom'),_,_) -> nom = nom')
    tr

(* Imprime l'état et les transitions qui
 * en partent dans le fichier f_out *)
let pr_transitions_depuis_etat
      ((_,nom):state) (tr_depuis_q:transitions) f_out =
  pr f_out nom;
  pr f_out " -> {";
  List.iter
    (fun (_,_,(_,nom)) -> pr f_out (nom^"; "))
    tr_depuis_q;
  pr f_out "}\n"

(* Imprime l'automate dans le fichier f_out *)
let pr_states_transitions
      (st_list:state list) (tr:transitions) etats_init etats_acceptant f_out =
  let rec aux (st_list:state list) =
    match st_list with
    | [] -> ()
    | ((q,nom) as h)::t ->
       let tr_depuis_q =
         transitions_depuis_etat h tr in
       pr_transitions_depuis_etat h tr_depuis_q f_out;
       aux t
  in
  pr f_out "digraph G{\n";
  pr_proprietes_etats st_list etats_init etats_acceptant f_out;
  aux st_list;
  pr f_out "}\n"
            
(* Même principe que la fonction ci-dessus
 * mais avec un automate en argument *)
let pr_automate ((st_list,_,tr,etats_init,etats_accept):automate) f_name =
  let f_out = open_dot_file f_name in
  pr_states_transitions st_list tr etats_init etats_accept f_out;
  close_out f_out
