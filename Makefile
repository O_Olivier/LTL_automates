# Non du fichier executable
EXEC=exec

DEP=syntax.cmo parser lexer automata.cmo automata2dot.cmo main.cmo

all: $(DEP)
	@ocamlc unix.cma syntax.cmo parser.cmo lexer.cmo automata.cmo automata2dot.cmo main.cmo -o $(EXEC)

# Règles spécifique à parser.mly
parser: parser.mly
	@echo "[Building parser]"
	@menhir --infer $^
	@echo "[Compiling parser.ml]"
	@ocamlc -c $@.mli $@.ml

# Règles spécifique à lexer.mll
lexer: lexer.mll
	@echo "[Building lexer]"
	@ocamllex $^
	@echo "[Compiling lexer.ml]"
	@ocamlc -c $@.ml

%.cmo %.cmi : %.ml %.mli
	@ocamlc -c -g $^

%.cmo: %.ml
	@echo "[Compiling $^]"
	@ocamlc -c -g $^

# Cleaning stuff
.PHONY: clean proper

clean:
	@rm *~

proper:
	@rm *.cm? $(EXEC) parser.ml parser.mli lexer.ml

