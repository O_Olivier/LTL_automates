open Printf
       
type formule =
  | True
  | False
  | Var of string
  | Not of formule
  | Or of formule * formule
  | And of formule * formule
  | Next of formule
  | Until of formule * formule
  | WeakUntil of formule * formule
  (*pour la logique du passé*)
  | Prev of formule
  | Since of formule * formule
  | WeakSince of formule * formule
 

(* Calcule la taille d'une formule *)
let rec taille = function
  | True | False | Var _-> 1
  | Not f | Next f |Prev f-> 1 + taille f
  | Or(f,g) | And(f,g) | Until(f,g)
    | WeakUntil(f,g) |Since(f,g) |WeakSince(f,g)  -> 1 + taille f + taille g

(* Retourne true si le connecteur principal
 * de la formule est G 
 * Il est assumé que la formule est sous nnf *)
let connecteur_principal_G : formule -> bool = function
  | WeakUntil(f,False) -> true
  | _ -> false

(* Retourne true si la formule est une conjonction
 * de G *)
let rec conjonction_G (phi:formule) : bool =
  match phi with
  | And(f,g) -> conjonction_G f && conjonction_G g
  | _ -> connecteur_principal_G phi
                            

(* Transforme une formule en sa forme normale negative i.e.
 * - Toutes les négations sont devant des propositions atomiques
 * - Seuls les opérateurs logiques true,false,OU,ET apparaissent
 * - Seuls les opérateurs temporels X,U et W apparaissent
 * - Tous les And(False,_) et And(_,False) sont remplacés par False
 * - Tous les And(True,f) et And(f,True) sont remplacés par f
 * - Tous les Or(True,_) et Or(_,True) sont remplacés par True
 * - Tous les Or(False,f) et Or(f,False) sont remplacés par f
*)
let forme_normale_neg phi =
  let rec nnf  : formule -> formule = function
    | True | False | Var _ as phi -> phi
    | Or(True,_) | Or(_,True) -> True
    | Or(False,f) | Or(f,False) -> nnf f
    | Or(f,g) -> Or(nnf f,nnf g)
    | And(False,_) | And(_,False) -> False
    | And(True,f) | And(f,True) -> nnf f
    | And(f,g) -> And(nnf f,nnf g)
    | Next f -> Next (nnf f)
    | Until(f,g) -> Until(nnf f,nnf g)
    | WeakUntil(f,g) -> WeakUntil(nnf f,nnf g)
    | Prev f -> Prev (nnf f)
    | Since(f,g) -> Since(nnf f,nnf g)
    | WeakSince(f,g) -> WeakSince(nnf f,nnf g)
    | Not f ->
       match f with
       | True -> False
       | False -> True
       | Var p -> Not (Var p)
       | Not f -> nnf f
       | Or(f,g) -> nnf (And(nnf (Not f),nnf (Not g)))
       | And(f,g) -> nnf (Or(nnf (Not f),nnf (Not g)))
       | Next f -> Next (nnf (Not f))
       | Until(f,g) -> WeakUntil(nnf (Not g),
                                 nnf (And(nnf (Not f), nnf (Not g))))
       | WeakUntil(f,g) -> Until(nnf (Not g),
                                 nnf (And(nnf (Not f), nnf (Not g))))
       (* Pourquoi tester le cas où f == True ? *)
       | Prev f -> if f==True then Not(Prev(True)) else Prev(nnf (Not f))
       | Since(f,g) -> WeakSince(nnf(Not g),
                                 nnf (And(nnf(Not f),nnf( Not g))))
       | WeakSince(f,g) -> Since(nnf (Not g),
                                 nnf (And(nnf (Not f), nnf (Not g))))
  in nnf phi
         

(* Transforme une conjonction de G en un
 * G de conjonction *)
let transforme_conj_G (psi:formule) : formule =
  let rec aux phi = match phi with
    | And(phi1,phi2) ->
       let f = match aux phi1 with
         | WeakUntil(f1,False) -> f1
         | _ -> failwith "pattern impossible"
       and g = match aux phi2 with
         | WeakUntil(g1,False) -> g1
         | _ -> failwith "pattern impossible"
       in
       let phi' = WeakUntil(And(f,g),False)
       in forme_normale_neg phi'
    |  _ -> phi
  in
  if conjonction_G psi then aux psi
  else psi

(* Idempotence *)
let rec idempotence phi = match phi with
  | Until(phi1, (Until(phi2,phi3) as psi) ) when phi1 = phi2 -> idempotence psi
  | Until( (Until(phi1,phi2) as psi), phi3) when phi2 = phi3 -> idempotence psi
  | WeakUntil(phi1,False) when connecteur_principal_G phi1 -> idempotence phi1
  | Not f -> Not(idempotence f)
  | Next f -> Next(idempotence f)
  | And(f,g) -> And(idempotence f,idempotence g)
  | Or(f,g) -> Or(idempotence f,idempotence g)
  | Until(f,g) -> Until(idempotence f,idempotence g)
  | WeakUntil(f,g) -> WeakUntil(idempotence f,idempotence g)
  | _ -> phi
                               
(* Absorption *)
let rec absorption phi = match phi with
  | Until(True, (WeakUntil(Until(True,_),False) as psi)) -> absorption psi
  | WeakUntil( (Until(True,WeakUntil(_,False)) as psi) ,False) -> absorption psi
  | Not f -> Not(absorption f)
  | Next f -> Next(absorption f)
  | And(f,g) -> And(absorption f,absorption g)
  | Or(f,g) -> Or(absorption f,absorption g)
  | Until(f,g) -> Until(absorption f,absorption g)
  | WeakUntil(f,g) -> WeakUntil(absorption f,absorption g)
  | _ -> phi

(* Autres simplifications
 * cf thèse de Oddoux p.20 *)
let rec simplifications phi = match phi with
  | Next True -> True
  | Until(_,False) -> False
  | And(phi1, Until(_,phi2)) when phi1 = phi2 -> simplifications phi1
  | And(phi1, WeakUntil(_,phi2)) when phi1 = phi2 -> simplifications phi1
  | WeakUntil(WeakUntil(phi1,phi2),False) -> WeakUntil(simplifications phi1,False)
  | Next(WeakUntil(Until(True,_), False) as phi1) -> simplifications phi1
  | Or(WeakUntil(Until(True,phi1),False),
       WeakUntil(Until(True,phi2),False)) ->
     WeakUntil(Until(True,Or(simplifications phi1,simplifications phi2)),False)
  | And(Next phi1,Next phi2) ->
     Next(And(simplifications phi1,simplifications phi2))
  | Not f -> Not (simplifications f)
  | Next f -> Next(simplifications f)
  | And(f,g) -> And(simplifications f,simplifications g)
  | Or(f,g) -> Or(simplifications f,simplifications g)
  | Until(f,g) -> Until(simplifications f,simplifications g)
  | WeakUntil(f,g) -> WeakUntil(simplifications f,simplifications g)
  | _ -> phi
(* Applique les simplifications précédantes 
 * sur une formule *)
let optimise_formule : formule -> formule =
  let aux phi =
    let phi1 = transforme_conj_G phi in
    let phi2 = idempotence phi1 in
    let phi3 = absorption phi2 in
    let phi4 = simplifications phi3 in
    phi4
  in
  (* Faire preuve de l'existence d'un point fixe *)
  let rec point_fixe_simplification phi =
    let phi' = aux phi in
    if phi = phi' then phi
    else point_fixe_simplification phi'
  in
  point_fixe_simplification

         
(******************** Fonctions d'affichage*********************)
         
let rec formule_to_string : formule -> string = function
  | True -> "True"
  | False -> "False"
  | Var p -> p
  | Not f -> "not "^(formule_to_string f)
  | Or(f1,f2) -> sprintf "(%s or %s)"
                          (formule_to_string f1)
                          (formule_to_string f2)
  | And(f1,f2) -> sprintf "(%s and %s)"
                          (formule_to_string f1)
                          (formule_to_string f2)
  | Next f -> "X"^(formule_to_string f)
  | Until(f1,f2) -> sprintf "(%s U %s)"
                            (formule_to_string f1)
                            (formule_to_string f2)
  | WeakUntil(f1,f2) -> sprintf "(%s W %s)"
                                (formule_to_string f1)
     (formule_to_string f2)
  | Prev f -> "P"^(formule_to_string f)
  | Since(f1,f2) -> sprintf "(%s S %s)" (formule_to_string f1) (formule_to_string f2)
  | WeakSince(f1,f2) -> sprintf "(%s S %s)" (formule_to_string f1) (formule_to_string f2)

let rec formule_liste_to_string = function
    [] -> ""
  | [x] -> formule_to_string x
  | h::t -> (formule_to_string h)^"; "
            ^(formule_liste_to_string t)
               
(* Affiche une liste *)
let print_list l print_fun =
  let rec aux = function
      [] -> ()
    | [x] -> print_fun x;
    | h::t -> print_fun h; print_string " ; ";
              aux t
  in print_string "[ ";
     aux l;
     print_string " ]"

let print_list_formulas fl =
  print_list
    fl
    (fun f -> print_string (formule_to_string f))
