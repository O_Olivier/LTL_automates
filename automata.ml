(* Construction de l'automate *)

open Syntax

type symbol = string

type name = string

type state = formule list * name
 and alphabet = symbol list
 and transitions = (state * formule list * state) list

(* 
 * Un automate de Büchi (non deterministe) est un quintuplet
 * (Q,Q0,E,d,F) où :
 * - Q est un ensemble d'états
 * - Q0 inclus dans Q sont les etats initiaux
 * - E est un ensemble de symboles (l'alphabet)
 * - d : Q x E -> P(Q) est la fonction de transition
 * - F inclus dans Q est l'ensemble des etats acceptant
 *)                                                    
type automate =
  state list
  * alphabet
  * transitions
  * state list
  * state list

(* Supprime les doublons d'un liste *)
let supprime_doublons l =
  let rec aux = function
      [] -> []
    | h::t -> if List.mem h t then aux t
              else h::(aux t)
  in aux l

(* Retourne l'ensemble des sous-formules
 * en les mettant sous nnf
 * Ex: (a U (not true)) devient :
 * [a; a U false; false; true] *)
let sous_formules (phi:formule) =
  let rec aux acc (f:formule) =
    match f with
    (* Constante *)
    | True | False | Var _ -> f::acc
                                   
    (* Unary op *)                  
    | Not g | Next g |Prev g-> aux (f::acc) g
                        
    (* Binary op *)
    | Or(g,h) | And(g,h) | Until(g,h)
      | WeakUntil(g,h) |Since(g,h) |WeakSince(g,h) -> (aux (f::acc) g)@(aux [] h)
  in
  let l = (List.map forme_normale_neg (aux [] phi)) in
  supprime_doublons l

(* Retourne les parties de taille <= Card(sous_form)
 * des sous-formules en rajoutant les négations 
 * de ces sous-formules
 * Ex: a ET true
 * devient (à ordre près) :
 *
 * [ [a; a ET true; true];
 *   [a; a ET true; false];
 *   [a; (non a) OU false; true];
 *   [a; (non a) OU false; false];
 *   [non a; a ET true; true];
 *   [non a; a ET true; false];
 *   [non a; (non a) OU false; true];
 *   [non a; (non a) OU false; false]  ]
 *)
let rec construit_parties_et_neg
          (sous_form:formule list) : formule list list =
  match sous_form with
  | [] -> []
  | [f] -> [[forme_normale_neg (Not f)];[f]]
  | h::t -> List.fold_left
              (fun (a:formule list list) f ->
                ((forme_normale_neg (Not h))::f)::(h::f)::a)
              []
              (construit_parties_et_neg t)

(* Met chaque formule sous nnf et supprime
 * les doublons pour chaque sous liste 
 * Ex : [ [true; a; true];
 *        [true; a; not true] ]
 *
 *      devient (à ordre près) :
 *      [ [true; a];
 *        [true; a; false] ]
*)
let met_sous_nnf_supprime_doublons (parties:formule list list) =
  let sous_nnf =
    List.map
      (fun f_list ->
        (List.map forme_normale_neg f_list))
      parties
  in
  let sans_doublons =
    List.map
      supprime_doublons
      sous_nnf
  in sans_doublons
    
(* Teste si un ensemble de formules E est cohérent i.e.
 *   - si (f ET g) dans E alors f et g dans E
 *   - si (f OU g) dans E alors f ou g dans E 
 *   - si (NON f) dans E alors f pas dans E
 *)
let coherent (f_list:formule list) =
  let coherent = function
    | True -> true
    | False -> false
    | Not f -> not (List.mem f f_list)
    | Or(f,g) ->  List.mem f f_list || List.mem g f_list
    | And(f,g) -> List.mem f f_list && List.mem g f_list
    | _ -> true
  in
  List.for_all coherent f_list

         
(* Teste si un ensemble de formule E est maximal i.e.
 * pour toute sous-formule f de la formule de base
 * soit f dans E, soit (non f) dans E
 * L'argument sous_form est l'ensemble des sous-formules
 * de la formule de base
 *)
let maximal (sous_form:formule list) (f_list:formule list) =
  List.for_all
    (fun f -> List.mem f f_list
              || List.mem (forme_normale_neg (Not f)) f_list)
    sous_form
    

(* Teste si un ensemble de formule E est conforme à la
 * sémantique de la LTL i.e.
 *   - si (f U g) dans E alors f dans E ou g dans E  (cond1)
 *   - si (f U g) est une sous-formule de la formule
 *   de base, alors g dans E => (f U g) dans E (cond2)
 *   - si (f S g) dans E alors f dans E ou g dans E (cond3)
 *   - si (f S g) est une sous-formule de la formule 
 *   de base, alors g (?) dans E => (f S g) dans E (cond4)
 * Idem pour W
 * L'argument sous_form est l'ensemble des sous-formules
 * de la formule de base
 *)
let conforme_semantique (sous_form:formule list) (f_list:formule list) =
  (* (f U g) dans E => f dans E ou g dans E *)
  let cond1 =
    List.for_all
      (fun phi -> match phi with
                  | Until(f,g) | WeakUntil(f,g) ->
                     List.mem f f_list || List.mem g f_list
                  | _ -> true)
      f_list

  (* si (f U g) une sous-formule de la formule de base,
   * alors g dans E => (f U g) dans E *)
  and cond2 =
    List.for_all
      (fun phi -> match phi with
                  | Until(f,g) | WeakUntil(f,g)->
                     (not (List.mem g f_list)) || List.mem phi f_list
                  | _ -> true)
      sous_form
  (*pour la logique du passé*)
  and cond3 =
    List.for_all
      (fun phi -> match phi with
      |Since(f,g) |WeakSince(f,g) -> List.mem f f_list || List.mem g f_list
      |_-> true)
      f_list
  and cond4 =
    List.for_all
      (fun phi -> match phi with
      |Since(f,g) |WeakSince(f,g) -> (not (List.mem g f_list)) || List.mem phi f_list
      |_-> true)
      sous_form
  in
  (* Pour être conforme on doit vérifier cond1 et cond2 *)
  cond1 && cond2 && cond3 && cond4

(* Optimisation pour G connecteur principal : on supprime 
 * tous les états qui contiennent Not phi *)
let optimisation_G phi partie =
  if connecteur_principal_G phi then
    List.filter
      (fun fl ->
        List.exists
          (fun f -> f = phi)
          fl)
      partie
  else
    partie

(* Inutilisé *)
(* Construit les états d'un automate qui reconnait
 * le langage mod(phi) (sans les noms des états) *)
      (*
let construit_etats_sans_noms (phi:formule) : formule list list =
  let sous_form = sous_formules phi in
  let l = construit_parties_et_neg sous_form in
  let l1 = met_sous_nnf_supprime_doublons l in
  let l2 = List.map (fun fl -> supprime_doublons (True::fl)) l1 in
  let l3 =
    List.filter
      (fun (q:formule list) ->
        coherent q
        && maximal sous_form q
        && conforme_semantique sous_form q
      )
      l2
  in
  optimisation_G phi l3
    
(* Construit les états d'un automate qui reconnait
 * le langage mod(phi) *)
let construit_etats (phi:formule) : state list =
  let etats_sans_noms = construit_etats_sans_noms phi in
  List.mapi
    (fun i q -> let nom = Printf.sprintf "q%d" i
                in (q,nom))
    etats_sans_noms
       *)
    
(* Teste si une formule est une proposition atomique *)
let rec est_prop_atomique : formule -> bool = function
  | Var _ -> true
  | Not g -> est_prop_atomique g
  | _ -> false

           
(* Teste si les états q et q' respectent la 
 * règle sur les Next i.e.
 * Pour tout (X f) dans les sous-formules
 * - (X f) dans q => f dans q'
 * - f dans q' => (X f) dans q
*)
let respecte_regle_next (sous_form:formule list) (q:formule list) (q':formule list) =
  List.for_all
    (fun phi -> match phi with
              | Next f -> ((not (List.mem phi q)) || List.mem f q')
                          && ((not (List.mem f q')) || List.mem phi q)
              | _ -> true)
    sous_form

    
(* Teste si les états q et q' respectent la 
 * règle sur les Until i.e.
 * Pour tout (f U g) dans les sous-formules
 * - (f U g) dans q => (g dans q OU (f dans q ET (f U g) dans q'))  (cond1)
 * - (g dans q OU (f dans q ET (f U g) dans q')) => (f U g) dans q  (cond2)
 * Idem pour W
*)
and respecte_regle_until (sous_form:formule list) (q:formule list) (q':formule list) =
  List.for_all
    (fun phi ->
      match phi with
      | Until(f,g) | WeakUntil(f,g) ->
         let cond1 =
           not (List.mem phi q)
           || (List.mem g q || (List.mem f q && List.mem phi q'))
         and cond2 =
           not (List.mem g q || (List.mem f q && List.mem phi q'))
           || List.mem phi q
         in
         cond1 && cond2
      | _ -> true)
    sous_form

(*pour la logique du passé*)
    
(* Teste si les états q et q' respectent la 
 * régle sur les Prev i.e. 
 * Pour tout (P f) dans les sous-formules
 * - (P f) dans q' => f dans q
 * - f dans q => (P f) dans q'
 *)
let respecte_regle_prev (sous_form:formule list) (q:formule list) (q':formule list) =
  List.for_all
    (fun phi -> match phi with
    | Prev f -> ( (not(List.mem phi q')) || (List.mem phi q) )&& ((not(List.mem phi q)) || (List.mem phi q'))
    |_ -> true
    )
    sous_form

(*mettre une régle en plus pour enlever les etats initiaux avec des prev ?*)

(* Teste si les états q et q' respecte la 
 * régle pour les Since i.e.
 * Pour tout (f S g) dans les sous-formules
 * -(f S g) dans q' => (f dans q && (f S g) dans q)  || g dans q'
 * -g dans q' ou (f dans q && (f S g) dans q) => (f S g) dans q'
 * Idem pour WeakSince
 *)

    
let respecte_regle_since (sous_form:formule list) (q:formule list) (q':formule list) =
  List.for_all
    (fun phi ->
      match phi with
      |Since(f,g) |WeakSince(f,g) ->
	 let cond1 =
	   not (List.mem phi q') || (List.mem phi q && List.mem f q) || List.mem g q'
	 and cond2 =
	   (not(List.mem g q') && not( List.mem f q && List.mem phi q) )|| List.mem phi q'
	 in
	 cond1 && cond2
      |_-> true)
    sous_form


(* Construit toutes les transitions en
 * provenance d'un état 
 * Une transition est un triplet (q,sigma,q')
 * où
 * - q,q' dans l'ensemble des états
 * - sigma est l'ensemble des propositions atomiques de q
*)
let construit_transitions_depuis_un_etat
      sous_form ((q,nom) as st:state) (st_list:state list): transitions =
  
  (* Construit les prop atom de q *)
  let sigma = List.filter est_prop_atomique q in
  
  (* Construit une transitions par états *)
  let transitions_possibles =
    List.map
      (fun st' -> (st,sigma,st'))
      st_list
  in
  (* Filtre pour ne garder que les bonnes transitions *)
  List.filter
    (fun (_,_,(q',_)) ->
      respecte_regle_next sous_form q q'
      && respecte_regle_until sous_form q q'
      && respecte_regle_prev sous_form q q'
      && respecte_regle_since sous_form q q' )
    transitions_possibles

                              
(* Construit l'ensemble des transitions *)
let construit_transitions sf (st_list:state list): transitions =
  List.fold_left
    (fun a q ->
      let t = construit_transitions_depuis_un_etat sf q st_list in
      List.rev_append t a)
    []
    st_list
    

(* Retourne les etats initiaux d'une formule *)
let etats_initiaux phi (st_list:state list) =
  List.filter
    (fun (q,nom) -> List.mem phi q
                    && not (List.exists (fun q -> match q with
                                                    Prev _ -> true
                                                  | _ -> false) q))
    st_list


(* Retourne les etats acceptant d'une formule *)
let etats_acceptant sous_form phi (st_list:state list) : state list list =
  let sous_formule_until =
    List.filter
      (fun f -> match f with
                | Until _ -> true
                | _ -> false)
      sous_form
  in
  (* L'ensemble F est un ensemble d'ensembles d'états *)
  let ensemble_f =
    List.map
      (fun phi ->
        match phi with
        | Until(_,g) ->
           List.filter
             (fun (q,_) ->
               List.mem (forme_normale_neg (Not phi)) q
               || List.mem g q
             )
             st_list
        | _ -> failwith "pattern matching : cas censé être impossible"
      )
      sous_formule_until
  in
  match ensemble_f with
    [] -> [st_list]
  | _ -> ensemble_f


(* Retourne la liste des états accessibles
 * en une seule transition depuis (q,nom) *)
let accessible_depuis ((q,nom):state) tr =
  let l = List.filter
            (fun ((_,nom'),_,st') -> nom=nom')
            tr
  in List.map (fun (_,_,st') -> st') l

(* Retourne la liste des états accessibles depuis
 * les états initiaux *)
let accessibles etats_init st_list tr =
  let rec aux acc = function
      [] -> acc
    | h::t ->
       let l = accessible_depuis h tr in
       let l1 = List.filter
                  (fun st -> not (List.mem st acc)
                             && not (List.mem st t)
                             && st <> h)
                  l
       in
       aux (h::acc) (List.rev_append l1 t)
  in aux [] etats_init

(* Retourne les transitions qui n'ont pas
 * comme départ ou comme arrivée un
 * état inaccessible *)
let filtre_tr_accessibles st_list tr =
  List.filter
    (fun (st,_,st') ->
      List.mem st st_list && List.mem st' st_list)
    tr
