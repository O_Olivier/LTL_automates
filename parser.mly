%{
open Syntax
%}

(* Variables propositionnelles *)
%token <string> STRING

(* Constantes *)
%token TRUE FALSE

(* Operateurs logique propositionnelle *)
%token NOT
%token AND OR IMPLIES

(* Operateurs LTL *)
%token NEXT UNTIL
%token EVENTUALLY ALWAYS
%token WEAKUNTIL

(* Operateurs PLTL *)
%token PREV
%token SINCE WEAKSINCE

%token R_PAR
%token L_PAR
%token EOF

%nonassoc NOT
%nonassoc NEXT
%nonassoc EVENTUALLY
%nonassoc ALWAYS
%nonassoc PREV

%right AND
%right OR
%right IMPLIES
%right UNTIL
%right WEAKUNTIL
%right SINCE
%right WEAKSINCE

%start <Syntax.formule> formule
%%

formule: e = expr; EOF { e }

expr:
  | c = constante      { c }
  | v = var	       { Var v }
  | e = unop_expr      { e }
  | e = binop_expr     { e }
  | L_PAR; e = expr; R_PAR { e }
  ;

constante:
  | TRUE               { True }
  | FALSE	       { Not True }
  ;

var: s = STRING	       { s }

unop_expr:
  | NOT; e = expr      { Not e }
  | NEXT; e = expr     { Next e }
  | EVENTUALLY; e = expr { Until(True,e) }
  | ALWAYS; e = expr   { Not(Until(True,Not e)) }
  | PREV; e = expr     { Prev e }
  ;

binop_expr:
  | e1 = expr; AND; e2 = expr
       { And(e1,e2) }
  | e1 = expr; OR; e2 = expr
       { Or(e1,e2) }
  | e1 = expr; UNTIL; e2 = expr
       { Until(e1,e2) }
  | e1 = expr; IMPLIES; e2 = expr
       { Not(And(e1,Not e2)) }
  | e1 = expr; WEAKUNTIL; e2 = expr
       { WeakUntil(e1,e2) }
  | e1 = expr; SINCE; e2 = expr
       { Since(e1,e2) }
  | e1 = expr; WEAKSINCE; e2 = expr
       { WeakSince(e1,e2) }
  ;