\documentclass{beamer}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}

\usepackage{syntax}
%\usepackage{verbatim}

\usepackage{tikz}
\usepackage{config-tikz}

\usepackage{color}

%\usepackage{amssymb}

\usepackage{amsthm}
\usepackage{amsmath}

% Pour produire le symbole ``= def''
\usepackage{mathtools}
\newcommand\eqdef{\stackrel{\mathclap{\normalfont\tiny{def}}}{=}}

\newtheorem{thm}{Théorème}

\usetheme{Warsaw}

\title{LTL et construction d'automate}
\author{Yanis Belkheyar et Olivier Martinot}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Structure de Kripke}
  Soit $AP$ un ensemble de propositions atomiques.
  Une structure de Kripke est un 4-uplet $(S,I,\to,L)$ où :
  \begin{itemize}
  \item $S$ est un ensemble fini d'états
  \item $I \subseteq S$ est l'ensemble des états initiaux
  \item $\to \subseteq S \times S$ est une relation de transition
  \item $L : S \to 2^{AP} $ est une fonction d'étiquetage
  \end{itemize}
  A partir de maintenant on notera $s \to s'$ au lieu de $(s,s')$.
\end{frame}

\begin{frame}{Exemple}
  \begin{figure}[H]
    \centering
    \begin{tikzpicture}
      \draw (-1,4.5) node {$\mathcal{S}$};
      \draw (0,3) node[rond,vert] (q0) {$q_0$} node[left=3mm] {$\{V\}$};
      \draw (0,0) node[rond,oorange] (q1) {$q_1$} node[left=3mm] {$\{O\}$};
      \draw (3,1.5) node[rond,rouge] (q2) {$q_2$} node[right=3mm] {$\{R\}$};

      \draw[-latex'] (q0) -- (q1);
      \draw[-latex'] (q1) -- (q2);
      \draw[-latex'] (q2) -- (q0);
    \end{tikzpicture}
  \end{figure}
\end{frame}

\begin{frame}{Exécution}
  Une exécution est une séquence infinie $s_0 s_1 ...$ telle que $s_0 \in I$ et $\forall 0 \leq i\ s_i \to s_{i+1}$
\end{frame}

\begin{frame}{Trace}
  La trace d'une exécution $\sigma = q_0 q_1 q_2 ...$ est $trace(\sigma) = L(q_0) L(q_1) L(q_2) ...$ où L est la fonction d'étiquettage
\end{frame}

\begin{frame}{Syntaxe de la LTL}
  \begin{grammar}
    <LTL> $\ni \phi$,$\psi$ ::= true | $p$ | $\neg \phi$ | $\phi \wedge \psi$
    | $X \phi$ | $\phi U \psi$
  \end{grammar}
  avec $p \in AP$
\end{frame}

\begin{frame}{Syntaxe de la PLTL}
  \begin{grammar}
    <PLTL> $\ni \phi$,$\psi$ ::= true | $p$ | $\neg \phi$ | $\phi \wedge \psi$
    | $X \phi$ | $\phi U \psi$ | $X^{-1} \phi$ | $\phi S \psi$
  \end{grammar}
  avec $p \in AP$
\end{frame}

\begin{frame}{Sémantique pour LTL}
  Soit $\varphi$ une formule LTL sur $AP$. On interprète $\varphi$ sur un mot infini $w = A_0 A_1 A_2 ...$ sur l'alphabet $2^{AP}$.
  La relation de satisfaction $\models \subseteq (2^{AP})^{\omega} \times LTL$ est définie inductivement par :
  \begin{itemize}
  \item $w \models a \in AP$ ssi $a \in A_0$
  \item $w \models \varphi_1 \wedge \varphi_2$ ssi $w \models \varphi_1$ et $w \models \varphi_2$
  \item $w \models \neg \varphi$ ssi $w \not\models \varphi$
  \item $w \models X \varphi$ ssi $w^1 \models \varphi$
  \item $w \models \varphi_1 U \varphi_2$ ssi $\exists j \geq 0\ w^j \models \varphi_2$ et $w^i \models \varphi_1 \forall 0 \leq i < j$
  \end{itemize}
\end{frame}

\begin{frame}{Sémantique pour PLTL}
  Soit $\varphi$ une formule PLTL sur $AP$. On interprète $\varphi$ sur une position $i$ le long d'un mot infini $w = A_0 A_1 A_2 ...$ sur l'alphabet $2^{AP}$. Le préfixe $w_i$ est le passé, le suffixe $w^i$ est le futur. La relation de satisfaction $\models$ est définie inductivement par :
  \begin{itemize}
  \item $w,i \models a \in AP$ ssi $a \in A_0$
  \item $w,i \models \varphi_1 \wedge \varphi_2$ ssi $w,i \models \varphi_1$ et $w,i \models \varphi_2$
  \item $w,i \models \neg \varphi$ ssi $w,i \not\models \varphi$
  \item $w,i \models X \varphi$ ssi $w,i+1 \models \varphi$
  \item $w,i \models X^{-1} \varphi$ ssi $i > 0$ et $w,i-1 \models \varphi$
  \item $w,i \models \varphi_1 U \varphi_2$ ssi $\exists i \leq j$ tel que $w,j \models \varphi_2$ et $w,k \models \varphi_1 \forall i \leq k < j$
  \item $w,i \models \varphi_1 S \varphi_2$ ssi $\exists 0 \leq j \leq i$ tel que $w,j \models \varphi_2$ et $w,k \models \varphi_1 \forall j < k \leq i$
  \end{itemize}
\end{frame}


\begin{frame}{Opérateurs supplémentaires}
  On peut également rajouter les opérateurs suivants : $F$, $G$, $W$ et $WS$
\end{frame}

\begin{frame}{Exemples}
  \begin{itemize}
  \item $G(F(v))$ ($\neq F(G(v))$)
  \item $G(v \implies X(o))$
  \item ...
  \end{itemize}
\end{frame}


%\begin{frame}{Forme normale négative}
%  Une formule est sous \emph{forme normale négative} si :
%  \begin{itemize}
%  \item les négations sont sous les opérateurs
%  \item seuls les opérateurs $\top$, $\bot$, $\wedge$, $\vee$, $X$, $U$, $W$, $X^{-1}$, $S$ et $WS$ apparaissent (en particulier il n'y a pas F ni G)
%  \end{itemize}
%\end{frame}


\begin{frame}{Satisfaisabilité}
  On dira qu'une formule $\varphi$ de LTL est satifaisable si:
  \begin{equation*}
    \exists w \in (2^{AP})^{\omega},\ tel\ que\ w\models \varphi
  \end{equation*}
  On dira qu'une formule $\psi$ de PLTL est satisfaisable si:
  \begin{equation*}
    \exists w \in (2^{AP})^{\omega} et\ \exists\ i \in \mathbb{N}\ tel\ que\ w,i\models \psi 
  \end{equation*}
  On dira qu'une formule $\psi$ de PLTL est initialement satisfaisable si:
  \begin{equation*}
    \exists w \in (2^{AP})^{\omega} tel\ que\ w,0\models\psi
    \end{equation*}
\end{frame}

\begin{frame}{Model d'une formule}
$Model_{LTL}(\varphi)=\{ w \in (2^{AP})^{\omega}\ |\ w \models \varphi \}$ pour la LTL \newline
  $Model_{PLTL}(\varphi)=\{(w,i) \in (2^{AP})^{\omega}\times\mathbb{N}\ |\ w,i \models \varphi \}$ pour la PLTL\newline
  $Model^0_{PLTL}(\varphi)=\{w\in (2^{AP})^{\omega}\ |\ w,0\models\varphi\}$
\end{frame}  

\begin{frame}{Model checking}
  Considerons une structure de Kripke M quelconque et une formule $\phi$ .
  \begin{itemize}
  \item Le model checking usuel (ou universel) consiste à savoir si :\newline
   \begin{equation*}
    Traces(M) \subseteq Model_{LTL}(\phi)\ ou\ Traces(M) \subseteq Model^0_{PLTL}(\phi)
  \end{equation*}
\item Le model checking existentiel consiste à savoir si : \newline
  \begin{equation*}
    Traces(M) \cap Model_{LTL}(\phi) = \emptyset\ ou\ Traces(M) \cap Model^0_{PLTL}(\phi)
  \end{equation*}
\end{itemize}
\end{frame}

\begin{frame}{Objectif de construction de l'automate}
    On va construire un automate afin de reconnaitre les modèles d'une formule de LTL ou les modèles initiaux d'une formule de PLTL 
\end{frame}

\begin{frame}{Automate de Büchi non deterministe (NBA)}
  Un automate de Büchi non deterministe $\mathcal{A}$ est un quintuple $(Q,\Sigma,\delta,Q_0,F)$ où :
  \begin{itemize}
  \item $Q$ est un ensemble fini d'états
  \item $\Sigma$ est l'alphabet
  \item $\delta : Q \times \Sigma \to 2^Q$ est une fonction de transition
  \item $Q_0 \subseteq Q$ est l'ensemble des états initiaux
  \item $F \subseteq Q$ est l'ensemble des états acceptant
  \end{itemize}
\end{frame}

\begin{frame}{Automate de Büchi généralisé (GNBA)}
  Un automate de Büchi généralisé non deterministe $\mathcal{A}$ est un quintuple $(Q,\Sigma,\delta,Q_0,F)$ où :
  \begin{itemize}
  \item $Q$ est un ensemble fini d'états
  \item $\Sigma$ est l'alphabet
  \item $\delta : Q \times \Sigma \to 2^Q$ est une fonction de transition
  \item $Q_0 \subseteq Q$ est l'ensemble des états initiaux
  \item \textcolor{red}{$F = \{F_1, ..., F_n\}\subseteq 2^Q$} est l'ensemble des états acceptant
  \end{itemize}
\end{frame}

\begin{frame}{Exécution}
  Une exécution $\sigma = q_0 q_1 q_2 ... \in \Sigma^\omega$ est une séquence infinie $q_0 q_1 q_2 ...$ d'états de $\mathcal{A}$ telle que $q_0 \in Q_0$ et $\forall 0 \leq i\ q_i \xrightarrow{A_i} q_{i+1}$.\newline
\end{frame}


\begin{frame}{Algorithme de construction de l'automate}
  Nous allons construire l'automate $A_{\Phi}$=($Q_{\Phi},2^{AP},\delta_{\Phi},Q_0^{\Phi},F_{\Phi}$) tel que:
  \begin{itemize}
  \item$Q_{\Phi}$ est l'ensemble des états de l'automate ; chaque état correspond à un sous-ensemble de sous-formules de $\Phi$ ou de négations de sous-formule de $\Phi$,
  \item$2^{AP}$ est l'ensemble des propositions atomiques de la formule $\Phi$,
  \item$\delta_{\Phi}$ est l'ensemble des transitions entre les états de $Q_{\Phi}$,
  \item$Q_0^{\Phi}$ est l'ensemble des états entrants de l'automate $A_{\Phi}$,
  \item$F_{\Phi}$ est l'ensemble des états acceptants de l'automate $A_{\Phi}$.
  \end{itemize}
\end{frame}


\begin{frame}{Règles de construction des états}
  \begin{itemize}
  \item[$\bullet$] cohérent : Soit $q \in Q$ un état, $\phi \in q$ si et seulement si $\neg\phi \notin q$ et si $\phi\vee \psi\in q$ alors $\phi\in q$ ou $\psi\in q$ et si $\phi\wedge \psi\in q$ alors $\phi\in q$ et $\psi\in q$
  \item[$\bullet$] maximaux : $\forall \psi \in Sub(\Phi) $ , $\forall q \in Q$ , $\psi \in q$ ou $\neg \psi \in q$
  \item[$\bullet$] respecte la sémantique
    \begin{itemize}
    \item si $\phi U\psi \in q$ alors $\phi\in q$ ou $\psi\in q$
    \item si $\phi U\psi\in Sub(\Phi)$, si $\psi\in q$ alors $\phi U\psi\in q$
    \item si $\phi S\psi\in$q alors $\phi\in q$ ou $\psi\in q$
    \item si $\phi S\psi\in Sub(\Phi)$, si $\psi\in q$ alors $\phi S\psi\in q$
    \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Règles de construction des transitions}
  Soient $q,q' \in Q$ et $\alpha \in 2^{AP}$.
  Il y a une transition $q \xrightarrow{\alpha} q'$, avec $\alpha = q \cap AP$ ssi :
 \begin{itemize}
\item $\forall X \varphi \in Sub(\phi)$, $X \varphi \in q \iff \varphi \in q'$
\item $\forall X^{-1} \varphi \in Sub(\phi)$, $\varphi \in q \iff X^{-1} \varphi \in q'$
\item $\forall \varphi_1 U \varphi_2 \in Sub(\phi)$, $\varphi_1 U \varphi_2 \in q \iff (\varphi_2 \in q \vee (\varphi_1 U \varphi_2 \in q'\wedge \varphi_1\in q))$
\item $\forall \varphi_1 S \varphi_2 \in Sub(\phi)$, $\varphi_1 S \varphi_2 \in q' \iff (\varphi_2 \in q' \vee (\varphi_1 S \varphi_2 \in q \wedge \varphi_1\in q))$
\end{itemize}
\end{frame}


\begin{frame}{Etats entrant}
  L'ensemble $Q_0$ des états initiaux d'un automate $A_{\Phi}$ est constitué des états $q \in Q$ tels que $\Phi \in q$.\newline
  Pour PLTL, il faut rajouter la condition que $q$ ne contient aucune formule de la forme $X^{-1} \varphi$.
\end{frame}

\begin{frame}{Etats acceptant}
  Pour chaque sous-formule $\varphi_1 U \varphi_2 \in SF(\Phi)$ définissons l'ensemble
  \begin{equation*}
    F_{\varphi_1 U \varphi_2} \eqdef \{ q \in Q\ |\ \varphi_1 U \varphi_2 \notin q \vee \varphi_2 \in q \}
  \end{equation*}
\end{frame}

\begin{frame}{Preuve de l'algorithme de construction}
  \begin{thm}
  Soit $A_\Phi = (Q_\Phi,2^{AP},\delta_\Phi,Q_0^\Phi,F_\Phi)$ l'automate de Büchi généralisé défini précédemment.\newline
  Soit $w=a_0a_1... \in (2^{AP})^{\omega}$, soit $\rho=q_0q_1...$ une exécution acceptante de $A_\Phi$ sur $w$, alors on a :
\end{thm}
\end{frame}

\begin{frame}{Simplifications de formules}
  \begin{equation*}
    G(\phi_1)\wedge...\wedge G(\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)
  \end{equation*}
  \begin{equation*}
    FF(\phi)\equiv F(\phi)\  et\ \  GG(\phi)\equiv G(\phi)
  \end{equation*}
  \begin{equation*}
    \phi_1U(\phi_1U\phi_2)\equiv\phi_1U\phi_2\  et\ \  (\phi_1U\phi_2)U\phi_2\equiv\phi_1U\phi_2
  \end{equation*}
  \begin{equation*}
    FGF(\phi)\equiv GF(\phi)\  et\  \ GFG(\phi)\equiv FG(\phi)
  \end{equation*}
  \begin{equation*}
    \phi_2\vee(\phi_1\wedge X(\phi_1U\phi_2))\equiv\phi_1U\phi_2
  \end{equation*}
  \begin{equation*}
    \phi\vee XF(\phi)\equiv F(\phi)\ et\  \  \phi\wedge XG(\phi)\equiv G(\phi)
  \end{equation*}
  \begin{equation*}
    \phi U\phi\equiv\phi
  \end{equation*}
  \begin{equation*}
    F(\phi_1)\wedge...\wedge F(\phi_n)\wedge G(\phi_1\wedge...\wedge\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)
  \end{equation*}
  \begin{equation*}
    (F(\phi_1)\vee...\vee F(\phi_n))\wedge G(\phi_1\wedge...\wedge\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)
  \end{equation*}
\end{frame}

\begin{frame}{Suppression des états non accessibles}
  On peut supprimer les états non accessibles afin de réduire le nombre
\end{frame}

\begin{frame}{Généralités}
  \begin{itemize}
  \item Ocaml
  \item Ocamllex et Menhir
  \item DOT
  \end{itemize}
\end{frame}

\begin{frame}
  \nocite{THESE}
  \nocite{PRINCIPLES}
  \bibliographystyle{plain-fr}
  \bibliography{rapport}
\end{frame}

\end{document}
