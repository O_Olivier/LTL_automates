\documentclass[titlepage]{article}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}

\usepackage{syntax}
\usepackage{verbatim}

\usepackage{amssymb}

\usepackage{float}
\usepackage{tikz}
\usepackage{config-tikz}

\usepackage{graphicx}

\usepackage{geometry}

\usepackage{amsthm}
\usepackage{amsmath}
\newtheoremstyle{break}
 {\topsep}{\topsep}%
 {\itshape}{}%
 {\bfseries}{}%
 {\newline}{}%
 \theoremstyle{break}
 
\newtheorem{thm}{Théorème}
\newtheorem{lemme}{Lemme}
\newtheorem{cor}[thm]{Corollaire}
\newtheorem{prop}{Proposition}

\theoremstyle{definition}
\newtheorem{defn}{Définition}

\theoremstyle{remark}
\newtheorem{rmq}{Remarque}

\theoremstyle{remark}
\newtheorem{notation}{Notation}

% Pour produire le symbole ``= def''
\usepackage{mathtools}
\newcommand\eqdef{\stackrel{\mathclap{\normalfont\tiny{def}}}{=}}

\begin{document}
\title{LTL et construction d'automate}
\author{Yanis Belkheyar et Olivier Martinot}
\date \today
\maketitle

\tableofcontents

\part{Introduction}
Nous nous sommes intéressés à ce sujet par curiosité : qu'est ce que la construction d'automate pour la logique temporelle de temps lineaire? C'est une très bonne question Jamy. La logique temporelle linéaire est une logique permettant d'exprimer des propriétés temporelles du futur comme : l'événement ``les étudiants de maths-info auront leurs résultats d'examen''  se produira un jour. Une extension que nous avons choisi de considérer en plus est la PLTL qui permet également d'exprimer des propriétés du passé comme : l'événement A se produit depuis que l'événement B s'est produit. De plus, pour savoir si une formule de logique temporelle linéaire est satifaisable nous allons avoir besoin de construire l'automate associé à la formule que nous souhaitons vérifier. Nous allons donc définir formellement la logique temporelle linéaire et donner un algorithme permettant de construire l'automate associé à une formule. Nous avons programmé cet algorithme et y avons ajouté quelques optimisations afin de le rendre plus efficace.
\newpage

\part{Définitions}

\section{Structure de Kripke}

\subsection{Structure de Kripke}

\begin{defn}[Structure de Kripke]
  Soit $AP$ un ensemble de propositions atomiques.
  Une structure de Kripke est un 4-uplet $(S,I,\to,L)$ où :
  \begin{itemize}
  \item $S$ est un ensemble fini d'états
  \item $I \subseteq S$ est l'ensemble des états initiaux
  \item $\to \subseteq S \times S$ est une relation de transition
  \item $L : S \to 2^{AP} $ est une fonction d'étiquetage
  \end{itemize}
  A partir de maintenant on notera $s \to s'$ au lieu de $(s,s')$ et on supposera que $\to$ est totale (i.e. $\forall s \in S, \exists s' \in S, s \to s'$)
\end{defn}


\subsection{Chemin}

\begin{defn}[Chemin]
  Un chemin fini de M est une séquence finie d'états $s_0 s_1 ... s_n$ telle que $\forall 0 \leq i < n\ s_i \to s_{i+1}$.
  Un chemin infini de M est une séquence infinie $s_0 s_1 ...$ telle que $\forall 0 \leq i\ s_i \to s_{i+1}$.
  Notons Path(s) l'ensemble des chemins partant de s.
\end{defn}


\begin{defn}[Exécution]
  Une exécution est un chemin commençant par un état initial.
  Notons Exec(M) l'ensemble des chemins infinis de M et Exec(s) l'ensemble des chemins infinis partant de s.
\end{defn}

\subsection{Trace}

\begin{defn}[Trace]
  Soit $M = (S,I,\to,L)$ une structure de Kripke. La \emph{trace} d'un chemin infini $\pi = s_0 s_1 ...$ est définie par $trace(\pi) = L(s_0) L(s_1) ...$.\newline
  $trace(\pi)$ est donc un mot infini sur l'alphabet $2^{AP}$.
  L'ensemble des traces d'un ensemble $\Pi$ de chemins est :\newline
  $trace(\Pi) = \{ trace(\pi) | \pi \in \Pi \}$.\newline
  Notons $Traces(s) = trace(Exec(s))$ et $Traces(M) = \bigcup_{s \in I}{Traces(s)}$
\end{defn}

%\subsection{Propriétés linéaire temporelle}

%\begin{defn}[Propriété linéaire temporelle (LT)]
%  Une \emph{propriéte linéaire temporelle} sur l'ensemble de propositions atomiques $AP$ est une partie de $(2^{AP})^\omega$.
%\end{defn}


%\begin{defn}[Relation de satisfaction pour propriétés LT]
%  Soit $P$ une propriété LT sur $AP$ et $M = (S,I,\to,L)$ une structure de Kripke.
%  M \emph{satisfait} $P$ (noté $M \models P$) ssi $Traces(M) \subseteq P$.
%\end{defn}


%\begin{thm}[Inclusion de trace et propriétés LT]
%  Soient $M$ et $M'$ des structures de Kripke sur le même ensemble $AP$ de propositions atomiques. Les propriétés suivants sont équivalents :
%  \begin{enumerate}
%  \item $Trace(M) \subseteq Trace(M')$
%  \item Pour toute propriété $P$ : $M' \models P \implies M \models P$
%  \end{enumerate}
%\end{thm}


%\begin{defn}[Equivalence de trace]
%  Deux structures de Kripke $M$ et $M'$ sont \emph{trace-equivalente} si $Trace(M) = Trace(M')$
%\end{defn}

%\begin{cor}[Equivalence de trace et propriété LT]
%  Soient $M$ et $M'$ deux structures de Kripke. Alors :
%  $Trace(M) = Trace(M') \iff M$ et $M'$ satisfont les même propriétés LT
%\end{cor}

\section{LTL et PLTL}

La logique temporelle linéaire est un formalisme qui permet de décrire de façon élégante des propriétés sur des mots infinis.
Le but est de définir des propriétés sur le futur (et sur le passé pour PLTL) d'un système pour vérifier qu'il est correct.


\subsection{Syntaxe de LTL et PLTL}
Pour ce faire, il nous faut introduire des opérateurs en plus de ceux de la logique propositionnelle :

\begin{itemize}
\item $X \phi$ pour ``demain $\phi$'', on aura $\phi$ demain
\item $\phi U \psi$ pour ``$\phi$ until $\psi$'', on a $\phi$ jusqu'à avoir $\psi$
\end{itemize}

Bien que ces opérateurs soient suffisants pour exprimer les propriétés du futur, on peut en rajouter d'autres :
\begin{itemize}
\item $F \phi$ pour ``enventually $\phi$'', un jour on aura $\phi$
\item $G \phi$ pour ``always $\phi$'', on a toujours $\phi$
\item $\phi W \psi$ pour ``$\phi$ weak until $\psi$'', on a $\phi$ jusqu'à avoir éventuellement $\psi$\newline
\end{itemize}

Fixons AP un ensemble de propositions atomiques.

\begin{defn}
  \begin{grammar}
    <LTL> $\ni \phi$,$\psi$ ::= true | $p \in AP$ | $\neg \phi$ | $\phi \wedge \psi$
    | $X \phi$ | $\phi U \psi$
  \end{grammar}
\end{defn}

Pour prendre en compte le passé également, il faut rajouter d'autres opérateurs :
\begin{itemize}
\item $X^{-1} \phi$ pour ``hier $\phi$'', on a eu $\phi$ hier
\item $\phi S \psi$ pour ``$\phi$ since $\psi$'', on a $\phi$ depuis qu'on a eu $\psi$
\end{itemize}
On peut, comme pour LTL, rajouter des opérateurs :
\begin{itemize}
  \item $\phi WS \psi$ pour ``$\phi$ weak since $\psi$'', on a $\phi$ depuis $\psi$ ou depuis toujours
\end{itemize}

On obtient alors une définition pour la syntaxe de PLTL :

\begin{defn}
  \begin{grammar}
    <PLTL> $\ni \phi$,$\psi$ ::= true | $p \in AP$ | $\neg \phi$ | $\phi \wedge \psi$
    | $X \phi$ | $\phi U \psi$ | $X^{-1} \phi$ | $\phi S \psi$
  \end{grammar}
\end{defn}


Nous pouvons alors definir inductivement une notion de taille d'une formule :

\begin{defn}[Taille d'une formule]
  Soit $\phi$ une formule de LTL ou PLTL. On définit la taille de $\phi$, que l'on note $|\phi|$, par :
  \begin{itemize}
  \item $|\phi| = 1$ si $\phi = true$ ou $\phi = p \in AP$
  \item $|\phi| = 1 + |\psi|$ si $\phi = \oplus \psi$ avec $\oplus \in \{ \neg, X, X^{-1} \}$
  \item $|\phi| = 1 + |\psi_1| + |\psi_2|$ si $\phi = \psi_1 \otimes \psi_2$ avec $\otimes \in \{ \wedge, U, S\}$
  \end{itemize}
\end{defn}


\subsection{Sémantique de LTL et PLTL}

\begin{notation}
  Soit un mot infini $w = A_0 A_1 A_2 ...$ sur un alphabet $\Sigma$. Notons $w^i$ le suffixe $A_i A_{i+1} ...$ et $w_i$ le préfixe (fini) $A_0 A_1 ... A_i$.
\end{notation}

\begin{defn}[Sémantique pour LTL]
  Soit $\varphi$ une formule LTL sur $AP$. On interprète $\varphi$ sur un mot infini $w = A_0 A_1 A_2 ...$ sur l'alphabet $2^{AP}$.
  La relation de satisfaction $\models \subseteq (2^{AP})^{\omega} \times LTL$ est définie inductivement par :
  \begin{itemize}
  \item $w \models a \in AP$ ssi $a \in A_0$
  \item $w \models \varphi_1 \wedge \varphi_2$ ssi $w \models \varphi_1$ et $w \models \varphi_2$
  \item $w \models \neg \varphi$ ssi $w \not\models \varphi$
  \item $w \models X \varphi$ ssi $w^1 \models \varphi$
  \item $w \models \varphi_1 U \varphi_2$ ssi $\exists j \geq 0\ w^j \models \varphi_2$ et $w^i \models \varphi_1 \forall 0 \leq i < j$
  \end{itemize}
\end{defn}
On utilisera également les abbreviations et operateurs usuels : $\bot$, $\top$, $\vee$, $\implies$, etc.
On peut définir la syntaxe pour $F$, $G$ et $W$ :
\begin{itemize}
\item $F \varphi \eqdef \top U \varphi$
\item $G \varphi \eqdef \neg F \neg \varphi$
\item $\varphi W \psi \eqdef (\varphi U \psi) \vee G \varphi$
\end{itemize}


\begin{defn}[Sémantique pour PLTL]
  Soit $\varphi$ une formule PLTL sur $AP$. On interprète $\varphi$ sur une position $i$ le long d'un mot infini $w = A_0 A_1 A_2 ...$ sur l'alphabet $2^{AP}$. Le préfixe $w_i$ est le passé, le suffixe $w^i$ est le futur. La relation de satisfaction $\models$ est définie inductivement par :
  \begin{itemize}
  \item $w,i \models a \in AP$ ssi $a \in A_0$
  \item $w,i \models \varphi_1 \wedge \varphi_2$ ssi $w,i \models \varphi_1$ et $w,i \models \varphi_2$
  \item $w,i \models \neg \varphi$ ssi $w,i \not\models \varphi$
  \item $w,i \models X \varphi$ ssi $w,i+1 \models \varphi$
  \item $w,i \models X^{-1} \varphi$ ssi $i > 0$ et $w,i-1 \models \varphi$
  \item $w,i \models \varphi_1 U \varphi_2$ ssi $\exists i \leq j$ tel que $w,j \models \varphi_2$ et $w,k \models \varphi_1 \forall i \leq k < j$
  \item $w,i \models \varphi_1 S \varphi_2$ ssi $\exists 0 \leq j \leq i$ tel que $w,j \models \varphi_2$ et $w,k \models \varphi_1 \forall j < k \leq i$
  \end{itemize}
\end{defn}
On peut également définir la sémantique pour $WS$ :
\begin{itemize}
\item $\varphi_1 WS \varphi_2 \eqdef \varphi_1 S \varphi_2 \vee \neg (\neg\varphi_1 S\ \top)$
\end{itemize}
On peut ainsi interpréter une formule LTL (resp. PLTL) sur une trace d'une exécution d'une structure de Kripke.
Notons que PLTL contient LTL et que leurs sémantiques sont compatibles : $w,i \models_{PLTL} \varphi \iff w^i \models_{LTL} \varphi$, $\forall \varphi \in LTL$, $\forall w \in (2^{AP})^{\omega}$, $\forall i \in \mathbb{N}$
 
Comme dans d'autres types de logique, il est pratique de définir la \emph{forme normale négative} d'une formule, c'est à dire une formule équivalente dans laquelle les négations sont propagées sous les opérateurs.

\begin{defn}[Forme normale négative]
  Une formule est sous \emph{forme normale négative} si :
  \begin{itemize}
    \item les négations n'apparaissent que devant des propositions atomiques
    \item seuls les opérateurs $\top$, $\bot$, $\wedge$, $\vee$, $X$, $U$, $W$, $X^{-1}$, $S$ et $WS$ apparaissent (en particulier les opérateurs F et G n'apparaissent pas)
  \end{itemize}
  Notons $NNF(\varphi)$ la forme normale négative de $\varphi$
\end{defn}


Nous nous servirons du fait suivant :

\begin{prop}[Existence et unicité de la forme normale négative]
  Une formule de LTL ou PLTL possède une unique forme normale négative.
\end{prop}

\begin{proof}
  Pour transformer une formule $\varphi \in PLTL$ en une formule où les négations n'apparaissent que devant des propositions atomiques, il suffit d'utiliser les lois de De Morgan et les équivalences suivantes :
  \begin{itemize}
  \item[$\bullet$] $\neg X(\varphi) \equiv X( \neg \varphi)$
  \item[$\bullet$] $\neg (\phi U \psi) \equiv (\neg \psi) W (\neg \phi \wedge \neg \psi)$
  \item[$\bullet$] $\neg (\phi W \psi) \equiv (\neg \psi) U (\neg \phi \wedge \neg \psi)$
  \item[$\bullet$] $\neg X^{-1}(\varphi) \equiv X^{-1} (\bot) \vee X^{-1}(\neg \varphi)$
  \item[$\bullet$] $\neg (\phi S \psi) \equiv (\neg \psi) WS (\neg \phi \wedge \neg \psi)$
  \item[$\bullet$] $\neg (\phi WS \psi) \equiv (\neg \psi) S (\neg \phi \wedge \neg \psi)$
  \end{itemize}
\end{proof}


\section{Exemples}
Prenons comme exemple le fonctionnement d'un feu tricolore. On peut poser comme ensemble de propositions atomiques $AP =  \{vert, orange, rouge \}$.
%Prenons comme ensembles de notre structure de Kripke : $AP = \{vert, orange, rouge \}$, $S = \{ s_0, s_1, s_2 \}$, $I = \{ s_0 \}$, $\to = S \times S$ et $L(s_0) = vert, L(s_1) = orange, L(s_2) = rouge$.\newline


Une propriété basique sur les feux peut être qu'il n'y en a qu'un seul allumé à chaque instant : $G((vert \wedge \neg orange \wedge \neg rouge) \vee (\neg vert \wedge orange \wedge \neg rouge) \vee (\neg vert \wedge \neg orange \wedge rouge))$.


Pour exprimer la propriété que le feu ne passe pas du vert au rouge, on peut écrire la formule $\neg F(vert \wedge X rouge)$. De même on peut exprimer la propriété que le feu est vert ou orange jusqu'à devenir rouge par $(vert \vee orange)\ U\ rouge$ ou encore que le feu est orange avant d'être rouge par $rouge\ S\ orange$. On peut également combiner les opérateurs $F$ et $G$ pour obtenir la propriété que le feu sera vert infiniment souvent dans le futur : $G(F(vert))$.


\textbf{\underline{Exemples :}}\newline
Voici deux structures de Kripke qui modélisent les formules données en exemple précedemment :

\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \draw (-1,4.5) node {$\mathcal{S}$};
    \draw (0,3) node[rond,vert] (q0) {V} node[left=3mm] {$q_0$};
    \draw (0,0) node[rond,oorange] (q1) {O} node[left=3mm] {$q_1$};
    \draw (3,1.5) node[rond,rouge] (q2) {R} node[right=3mm] {$q_2$};

    \draw[-latex'] (q0) -- (q1);
    \draw[-latex'] (q1) -- (q2);
    \draw[-latex'] (q2) -- (q0);
  \end{tikzpicture}
  %\caption{Figure CVXVCXVCX.}
  %\label{fig-truc}
\end{figure}

\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \draw (-1,4.5) node {$\mathcal{S}'$};
    \draw (0,3) node[rond,vert] (q0) {V} node[left=3mm] {$q_0$};
    \draw (0,0) node[rond,vert] (q1) {V} node[left=3mm] {$q_1$};
    \draw (2,1.5) node[rond,oorange] (q2) {O} node[above=3mm] {$q_2$};
    \draw (4,3) node[rond,rouge] (q3) {R} node[right=3mm] {$q_3$};
    \draw (4,0) node[rond,rouge] (q4) {R} node[right=3mm] {$q_4$};
    \draw[-latex'] (q0) -- (q1);
    \draw[-latex'] (q0) -- (q2);
    \draw[-latex'] (q1) -- (q2);
    \draw[-latex'] (q2) -- (q4);
    \draw[-latex'] (q4) -- (q3);
    \draw[-latex'] (q3) -- (q0);
  \end{tikzpicture}
  %\caption{Figure HKJHKJHK.}
  %\label{fig-truc}
\end{figure}



\part{Problèmes de vérification}

Prenons une formule $\varphi$ de LTL, $\psi$ de PLTL et une structure de Kripke K :\newline

On dira qu'une formule $\varphi$ de LTL est satifaisables si:
\begin{equation*}
  \exists w \in (2^{AP})^{\omega},\ tel\ que\ \rho \models \varphi
\end{equation*}
On dira qu'une formule $\psi$ de PLTL est satisfaisable si:
\begin{equation*}
  \exists w \in (2^{AP})^{\omega} et\ \exists\ i \in \mathbb{N}\ tel\ que\ w,i\models \psi 
\end{equation*}

\begin{defn}[Modèle d'une formule]
  On appelle les \emph{modèles} de $\varphi$ une formule de LTL l'ensemble des mots infinis satisfaisant $\varphi$.\newline
  On appelle les \emph{modèles} d'une formule $\varphi$ de PLTL l'ensemble des couple de mots infinis $w$ et d'entier $i$ tel que $w,i\models \varphi$.\newline
  On appelle les \emph{modèles initiaux} d'une formule $\varphi$ de PLTL l'ensemble des mots infinis $w$ tels que $w,0\models\varphi$.\newline
  Notons : $Model_{LTL}(\varphi)=\{ w \in (2^{AP})^{\omega}\ |\ w \models \varphi \}$ pour la LTL \newline
  $Model_{PLTL}(\varphi)=\{(w,i) \in (2^{AP})^{\omega}\times\mathbb{N}\ |\ w,i \models \varphi \}$ pour la PLTL\newline
  $Model^0_{PLTL}(\varphi)=\{w\in (2^{AP})^{\omega}\ |\ w,0\models\varphi\}$
\end{defn}

\begin{defn}[Satisfaisabilité]
  On dit que $\varphi$ est \emph{satisfaisable} si $Model_{LTL}(\varphi) \neq \emptyset$ ou $Model_{PLTL}(\varphi) \neq \emptyset$
\end{defn}

\begin{defn}[Model checking]
Considerons une structure de Kripke M quelconque et une formule $\phi$ .
\begin{itemize}
\item Le model checking usuel (ou universel) consiste à savoir si :\newline
  \begin{equation*}
    Traces(M) \subseteq Model_{LTL}(\phi)\ ou\ Traces(M) \subseteq Model^0_{PLTL}(\phi)
  \end{equation*}
\item Le model checking existentiel consiste à savoir si : \newline
  \begin{equation*}
    Traces(M) \cap Model_{LTL}(\phi) = \emptyset\ ou\ Traces(M) \cap Model^0_{PLTL}(\phi)
  \end{equation*}
\end{itemize}
\end{defn}




%A ENLEVER POUR APRES
%\begin{comment}
\part{Automates pour LTL et PLTL}

\begin{defn}[Automate de Büchi non deterministe (NBA)]
  Un automate de Büchi non deterministe $\mathcal{A}$ est un quintuple $(Q,\Sigma,\delta,Q_0,F)$ où :
  \begin{itemize}
  \item $Q$ est un ensemble fini d'états
  \item $\Sigma$ est l'alphabet
  \item $\delta : Q \times \Sigma \to 2^Q$ est une fonction de transition
  \item $Q_0 \subseteq Q$ est l'ensemble des états initiaux
  \item $F \subseteq Q$ est l'ensemble des états acceptant
  \end{itemize}
  Une exécution $\sigma = A_0 A_1 A_2 ... \in \Sigma^\omega$ est une séquence infinie $q_0 q_1 q_2 ...$ d'états de $\mathcal{A}$ telle que $q_0 \in Q_0$ et $\forall 0 \leq i\ q_i \xrightarrow{A_i} q_{i+1}$.\newline
  L'exécution $q_0 q_1 q_2 ...$ est \emph{acceptant} si $q_i \in F$ pour un nombre infini d'indice $i \in \mathbb{N}$.\newline
  Le \emph{langage accepté} par $\mathcal{A}$ est :
  \begin{equation*}
    \mathcal{L}_\omega(\mathcal{A}) = \{ \sigma \in \Sigma^\omega |\ il\ y\ a\ une\ exécution\ acceptante\ pour\ \sigma\ dans\ \mathcal{A}\}
  \end{equation*}
  La taille de $\mathcal{A}$ notée $|\mathcal{A}|$ est le nombre d'états et de transitions de $\mathcal{A}$.
\end{defn}


\begin{defn}{Automate de Büchi généralisé (GNBA)}
  Un automate de Büchi généralisé $\mathcal{G}$ est un quintuple $(Q,\Sigma,\delta,Q_0,\mathcal{F})$ où $Q,\Sigma,\delta$ et $Q_0$ sont définis comme pour un NBA et $\mathcal{F}$ est une partie de $2^Q$.\newline
  Une exécution $q_0 q_1 q_2 ...$ est acceptant si $\forall F \in \mathcal{F}\ q_j \in F$ pour une infinité de $j \in \mathbb{N}$
\end{defn}


\section{Algorithme de construction d'automates}
Prenons une formule $\Phi$ de PLTL quelconque.\newline
On notera $Sub(\Phi)$:= l'ensemble des sous formule de $\Phi$ et l'ensemble des négations des sous formules de $\Phi$.\newline
Nous allons construire l'automate $A_{\Phi}$=($Q_{\Phi},2^{AP},\delta_{\Phi},Q_0^{\Phi},F_{\Phi}$) tel que:
\begin{itemize}
\item$Q_{\Phi}$ est l'ensemble des états de l'automate ; chaque état correspond à un sous-ensemble de sous-formules de $\Phi$ ou de négations de sous-formule de $\Phi$,
\item$2^{AP}$ est l'ensemble des propositions atomiques de la formule $\Phi$,
\item$\delta_{\Phi}$ est l'ensemble des transitions entre les états de $Q_{\Phi}$,
\item$Q_0^{\Phi}$ est l'ensemble des états entrants de l'automate $A_{\Phi}$,
\item$F_{\Phi}$ est l'ensemble des états acceptants de l'automate $A_{\Phi}$.
\end{itemize}

\subsection{Construction des états de l'automates} 
Ensuite nous allons construire les états de notre automate, ils seront \emph{cohérents}, \emph{maximaux} et \emph{consistant}, les états de $A_{\Phi}$ sont donc des ensembles de formules. L'idée de la construction est la suivante: un mot accepté sera un modèle de $\Phi$, et lorsqu'une execution acceptante passe par un état q à la position i, cela signifie que les formules de q sont vrais pour le suffixe i.\newline
\begin{itemize}
  \item cohérent : Soit $q \in Q$ un état, $\phi \in q$ si et seulement si $\neg\phi \notin q$ et si $\phi\vee \psi\in q$ alors $\phi\in q$ ou $\psi\in q$ et si $\phi\wedge \psi\in q$ alors $\phi\in q$ et $\psi\in q$
  \item maximaux : $\forall \psi \in Sub(\Phi) $, $\forall q \in Q$, $\psi \in q$ ou $\neg \psi \in q$
\end{itemize}
En plus de cela il faut rajouter des régles de construction afin que les états respectent la sémantique de la PLTL
\begin{itemize}
\item si $\phi U\psi \in q$ alors $\phi\in q$ ou $\psi\in q$
\item si $\phi U\psi\in Sub(\Phi)$, si $\psi\in q$ alors $\phi U\psi\in q$
\item si $\phi S\psi\in$q alors $\phi\in q$ ou $\psi\in q$
\item si $\phi S\psi\in Sub(\Phi)$, si $\psi\in q$ alors $\phi S\psi\in q$
\end{itemize}

\subsection{Transitions}
Soient $q,q' \in Q$ et $\alpha \in 2^{AP}$.
Il y a une transition $q \xrightarrow{\alpha} q'$, avec $\alpha = q \cap AP$ ssi :
\begin{itemize}
\item $\forall X \varphi \in Sub(\phi)$, $X \varphi \in q \iff \varphi \in q'$
\item $\forall X^{-1} \varphi \in Sub(\phi)$, $\varphi \in q \iff X^{-1} \varphi \in q'$
\item $\forall \varphi_1 U \varphi_2 \in Sub(\phi)$, $\varphi_1 U \varphi_2 \in q \iff (\varphi_2 \in q \vee (\varphi_1 U \varphi_2 \in q'\wedge \varphi_1\in q))$
\item $\forall \varphi_1 S \varphi_2 \in Sub(\phi)$, $\varphi_1 S \varphi_2 \in q' \iff (\varphi_2 \in q' \vee (\varphi_1 S \varphi_2 \in q \wedge \varphi_1\in q))$
\end{itemize}

\subsection{Etats entrant, états acceptant}
\subsubsection{Etats entrant}
L'ensemble $Q_0$ des états initiaux d'un automate $A_{\Phi}$ est constitué des états $q \in Q$ tels que $\Phi \in q$. Pour PLTL, il faut rajouter la condition que $q$ ne contient aucune formule de la forme $X^{-1} \varphi$ (car initialement ces formules sont toujours fausses).

\subsubsection{Etats acceptant}
Pour chaque sous-formule $\varphi_1 U \varphi_2 \in SF(\Phi)$ définissons l'ensemble
\begin{equation*}
  F_{\varphi_1 U \varphi_2} \eqdef \{ q \in Q\ |\ \varphi_1 U \varphi_2 \notin q \vee \varphi_2 \in q\}
\end{equation*}

Définissons $F_{\Phi} = \bigcup_{\varphi_1 U \varphi_2 \in SF(\Phi)}{F_{\varphi_1 U \varphi_2}}$ et
(on a $F \subseteq 2^Q$).\newline
L'ensemble $F$ est l'ensemble des états acceptant (c'est un ensemble d'ensembles).

\subsection{Complexité}
Le problème de model-checking est un problème qui fait partie de la classe des problèmes P-SPACE complet.\newline
On peut de plus voir que la construction de l'automate associé à une formule $\varphi$ est exponentielle en la taille de la formule. En effet on a $|Sub(\varphi)|=2\times|\phi|$ et le nombre d'états $|Q| = 2^{|Sub(\varphi)|}$

\subsection{Exemple}
\begin{figure}[H]
  \centering
  \begin{tikzpicture}
    \draw (-1,4.5) node {$\mathcal{A}$};
    \draw (0,3) node[rond,vert] (q0) {$q_0$} node[left=3mm] {};
    \draw (0,0) node[rond,double] (q1) {$q_1$} node[left=3mm] {};
    \draw (3,1.5) node[rond,double,vert] (q2) {$q_2$} node[right=3mm] {};
    
    \path[-latex'] (q0) edge [loop above] node[above] {$\neg vert$} (q0);
    \path[-latex'] (q0) edge node[below] {$\neg vert$} (q2);
    \path[-latex'] (q2) edge node[above] {$vert$} (q0);
    \path[-latex'] (q2) edge node[above] {$vert$} (q1);
    \path[-latex'] (q2) edge [loop above] node[above] {$vert$} (q2);
    \path[-latex'] (q1) edge [loop above] node[above] {$\neg vert$} (q1);
  \end{tikzpicture}
  \caption{Automate pour la formule $F(vert)$}
  %\label{fig-truc}
\end{figure}
Les états entrant sont representés en vert et les états acceptant sont representés en double cercle.


Des exécutions acceptantes possibles sont :
\begin{itemize}
\item $\sigma_1 = q_0 q_0 q_0 q_2 q_1 q_1 q_1 ...$ avec $trace(\sigma_1) = \{ \neg vert \}  \{ \neg vert \}  \{ \neg vert \}  \{ vert \}  \{ \neg vert \}  \{ \neg vert \} ...$
\item $\sigma_2 = q_2 q_2 q_2 ...$ avec $trace(\sigma_2) =  \{ vert \}  \{ vert \}  \{ vert \} ...$
\item $\sigma_3 = q_2 q_0 q_2 q_0 ...$ avec $trace(\sigma_3) = \{ vert \}  \{ \neg vert \}  \{ vert \}  \{ \neg vert \} ...$
\item ...
\end{itemize}


\section{Preuve de l'aglorithme de construction}
\begin{thm}
  Soit $A_\Phi = (Q_\Phi,2^{AP},\delta_\Phi,Q_0^\Phi,F_\Phi)$ l'automate de Büchi généralisé défini précédemment.\newline
  Soit $w=a_0a_1... \in (2^{AP})^{\omega}$, soit $\rho=q_0q_1...$ une exécution acceptante de $A_\Phi$ sur $w$, alors on a :
  
  \begin{equation*} 
    \forall \ i\ \geq \ 0\ ,\forall \phi \in \ Sub(\Phi)\ :\ (\varphi \in\ q_i\leftrightarrow w,i \models \varphi)
  \end{equation*}
\end{thm}
\begin{proof}
  Nous allons procéder par induction sur la taille de $\phi$ :
  \begin{itemize}
    
  \item Si $\varphi \in 2^{AP}$ : si $\varphi \in q_i$ alors par construction des transitions de l'automate $\varphi \in a_i$ donc on a bien que $w,i\models\varphi$.\newline
    De même si $w,i\models\varphi$ alors par construction des transitions de l'automate $\varphi\in q_i$.
    
  \item Si $\varphi=\phi_1\wedge\phi_2$ : Si $\phi_1\wedge\phi_2 \in q_i$ alors par construction on a que $\phi_1 \in q_i$ et $\phi_2\in q_i$ or par hypothèse d'induction on a que $w,i\models\phi_1 $ et $w,i\models\phi_2$ donc $w,i\models(\phi_1\wedge\phi_2)$.\newline
    Si $w,i\models(\phi_1\wedge\phi_2)$ alors $w,i\models\phi_1 $ et $w,i\models\phi_2$, donc par hypothése d'induction $\phi_1 \in q_i$ et $\phi_2\in q_i$ et par construction des états de façon cohérente on a bien que $\phi_1\wedge\phi_2 \in q_i$.\newline
    On procéde de la même façon pour $\phi_1\vee\phi_2$.
  \item Si $\varphi=\neg\varphi_1$ : si $\neg \varphi_1 \in q_i$ alors $\varphi_1\notin q_i$ donc par hypothése d'induction $w,i\not\models\varphi_1$ donc par cohérence $w,i\models\neg\varphi_1$.
      \newline
    Si $w,i\models\neg\varphi_1$, alors $w,i\not\models\varphi_1$. Par hypothése d'induction on a donc $\varphi_1\notin q_i$ et donc (par maximalité des états) on a $\neg\varphi_1 \in q_i$.
    
  \item Si $\varphi=X\psi$ : si $X\psi \in q_i$ alors par constructionansitions $\psi \in q_{i+1}$ donc $w,i+1\models \psi$ et ainsi $w,i\models X\psi$.\newline
    Si $w,i\models X\psi$ alors $w,i+1\models\psi$ et donc par hypothése d'induction $\psi \in q_{i+1}$ et donc par construction des transitions $X\psi \in q_i$.\newline
    
  \item Si $\varphi=X^{-1}\psi$ : si $X^{-1}\psi \in q_i$ alors $i>0$ car $q_0\in Q_0$ et ne peut contenir aucune formule de la forme $X^{-1}\psi$. On en déduit $\psi\in q_{i-1}$ et par hypothése d'induction $w,i-1\models\psi$ donc $w,i\models X^{-1}\psi$.\newline
    Si $w,i\models X^{-1}\psi$ alors $i>0$ et $w,i-1\models\psi$ et donc par hypothése d'induction $\psi \in q_{i-1}$ donc par construction $X^{-1}\psi \in q_i$

  \item Si $\varphi=\psi_1U\psi_2$ : si $\phi_1U\phi_2 \in q_i$, soit $\phi_2 \in q_i$ alors on a bien que $w,i \models \phi_2$ par hypothése d'iduction et donc $w,i\models \varphi$. Prenons le cas ou $\phi_2\notin q_i$; on a donc que par construction $\phi_1\in q_i$ et $\phi_1U\phi_2\in q_{i+1}$ et on sait aussi que $\rho$ est acceptante, donc $\exists \alpha_{min}\geq i+1$ tel que $q_{\alpha_{min}}$ est le premier état acceptant suivant $q_i$ autrement dit tel que $\phi_2\in q_{\alpha_{min}}$. Alors $\forall i\leq s<\alpha_{min},\ \phi_1U\phi_2 \in q_s$, en particulier $\phi_1U\phi_2 \in q_{i+1}$. Donc on a bien que $w,i\models\varphi$. \newline
    Si $w,i\models\varphi$ alors,par définition $\exists k\geq i$ avec $w,k\models\phi_2$ et $\forall i \leq j <k$ on a $w,j\models\phi_1$. Par hypothése d'induction on en deduit que $\phi_2\in q_k$ et $\phi_1\in q_j$. De plus $\phi_2\in q_k$, on en deduit que $\varphi\in q_k$(par construction des états), et ainsi $\forall i\leq p\leq k,\ \varphi\in q_p$. Donc $\varphi\in q_i$.
    
  \item Si $\varphi=\phi_1S\phi_2$ : si $\varphi\in q_i$, soit $\phi_2 \in q_i$ alors $\varphi$ est satisfaite donc $w,i\models \varphi$.
    Sinon, $\exists\alpha_0\geq 0$ tel que $\varphi_2 \in q_{\alpha_0}$ et donc $w,\alpha_0\models\varphi$. On a donc que $\forall j<s\leq i,\ \varphi\in q_s$. Donc on a bien que $\phi_1S\phi_2 \in q_{i-1}$ et $w,i-1\models \phi_1S\phi_2$, ainsi $w,i\models \phi_1S\phi_2$.\newline
    Si $w,i\models \phi_1S\phi_2$, alors soit $w,i\models \phi_2$ et donc $\phi_2 \in q_i$ par hypothése d'induction et donc $\varphi \in q_i$ par construction des états, soit $w,i\models\phi_1$ et $w,i-1\models\varphi$, donc par hypothése d'induction $\phi_1\in qi$ et donc par construction des états$\varphi \in q_i$.
  \end{itemize}
\end{proof}


\section{Optimisations}
%\subsection{Simplifications}
Pour optimiser la constructions de l'automate, nous appliquons tout d'abord un certain nombre de simplifications afin de diminuer la taille de la formule .
\begin{equation*}
  G(\phi_1)\wedge...\wedge G(\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)
\end{equation*}
\begin{equation*}
  FF(\phi)\equiv F(\phi)\  et\ \  GG(\phi)\equiv G(\phi)
\end{equation*}
\begin{equation*}
  \phi_1U(\phi_1U\phi_2)\equiv\phi_1U\phi_2\  et\ \  (\phi_1U\phi_2)U\phi_2\equiv\phi_1U\phi_2
\end{equation*}
\begin{equation*}
  FGF(\phi)\equiv GF(\phi)\  et\  \ GFG(\phi)\equiv FG(\phi)
\end{equation*}
\begin{equation*}
  \phi_2\vee(\phi_1\wedge X(\phi_1U\phi_2))\equiv\phi_1U\phi_2
\end{equation*}
\begin{equation*}
  \phi\vee XF(\phi)\equiv F(\phi)\ et\  \  \phi\wedge XG(\phi)\equiv G(\phi)
\end{equation*}
\begin{equation*}
  \phi U\phi\equiv\phi
\end{equation*}
\begin{equation*}
  F(\phi_1)\wedge...\wedge F(\phi_n)\wedge G(\phi_1\wedge...\wedge\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)
\end{equation*}
\begin{equation*}
  (F(\phi_1)\vee...\vee F(\phi_n))\wedge G(\phi_1\wedge...\wedge\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)
\end{equation*}

Nous allons nous contenter de faire la preuve de ces régles pour les deux dernières formules :
\begin{proof}
  Montrons que $R=F(\phi_1)\wedge...\wedge F(\phi_n)\wedge G(\phi_1\wedge...\wedge\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)$ :\newline
  Soit $w$ un mot tel que $w\models R$, alors $\forall i\in\{1,...,n\}\ w\models\phi_i$ car $w\models G(\phi_1\wedge...\wedge\phi_n)$.
  Donc $\forall i\ w\models F(\phi_i)$ et on a bien que si $w\models R$ alors $w\models G(\phi_1\wedge...\wedge\phi_n)$.\newline
  La démonstration de $ (F(\phi_1)\vee...\vee F(\phi_n))\wedge G(\phi_1\wedge...\wedge\phi_n)\equiv G(\phi_1\wedge...\wedge\phi_n)$ se fait de la même façon .
\end{proof}
De plus afin de minimiser le nombre d'états de l'automate nous supprimons tous les états non accessibles.

\section{Le programme}

\subsection{Généralités}
Afin de faciliter l'utilisation de notre programme nous avons fait un parser de formules, et nous écrivons l'automate obtenu dans le langage DOT (langage de description de graphes dans un format texte). De plus, nous mettons à disposition quelques fichiers tests contenant des formules plus ou moins grandes.


Voici les principaux outils utilisés pour ce projet :
\begin{itemize}
\item Ocaml pour le programme
\item Ocamllex et Menhir pour l'analyse lexical et syntaxique des formules
\item DOT pour la visualisation des automates\newline
\end{itemize}


Le code est reparti en 6 fichiers et comporte environ 2700 lignes :
\begin{itemize}
\item lexer.ml
\item parser.ml
\item syntax.ml pour la syntaxe de LTL et PLTL
\item automata.ml pour la construction de l'automate
\item automata2dot.ml pour l'écriture d'un automate dans un fichier .dot
\item main.ml pour l'exécution du programme en fonction des arguments de la ligne de commande
\end{itemize}


\subsection{Exemples d'utilisation du programme}
\begin{figure}[h]
  \includegraphics[scale=0.5]{ex1.png}
  \caption{Exécution du programme sur la formule $F(a)$}
\end{figure}

\begin{figure}[h]
  \includegraphics[scale=0.5]{ex1aut.png}
  \caption{Automate obtenu (en vert les états entrant, en cercle double les états acceptant)}
\end{figure}

\newgeometry{margin=1.1cm}
\subsection{Performances}
Le temps d'éxecution dans le tableau correspond à une moyenne sur 5 exécutions.
\newline
\newline
\begin{tabular}{|c|c|c|}
  \hline
   & Sans optimisations & Avec optimisations\\
  \hline
  Nombre de fichiers traités & 27 & 27\\
  \hline
  Pourcentage de réduction du nombre d'états & $0.00 \%$ & $52.46 \%$\\
  \hline
  Pourcentage de réduction de la taille des formules & $0.00 \%$ & $20.86 \%$\\
  \hline
  Temps d'éxecution & 0.612 s & 0.292 s \\
  \hline
\end{tabular}
\newline
\newline


On constate donc une nette amélioration lorsque l'on effectue des optimisations.\newline


Dans chacune des figures ci-dessous, la première exécution est sans optimisations et la deuxième est avec optimisations.
On peut voir sur ces figures que les optimisations sont plus ou moins efficaces selon la formule traitée. Ainsi on peut voir sur la figure \ref{fig-1} que les optimisations réduisent le nombre d'états et la taille de la formule, tandis que sur la figure \ref{fig-2} elles réduisent uniquement le nombre d'états, et que sur la figure \ref{fig-3} aucune optimisation n'est faite.


\begin{figure}[h]
  \includegraphics[scale=0.48]{f1.png}
  \caption{Exécution du programme avec et sans optimisation sur la formule $G(F(a)) \wedge G(F(b)) \wedge G(F(c))$}
  \label{fig-1}
\end{figure}

\begin{figure}[h]
  \includegraphics[scale=0.48]{f2.png}
  \caption{Exécution du programme avec et sans optimisation sur la formule $G(a) \wedge F(b) \vee F(a \implies c)$}
  \label{fig-2}
\end{figure}

\begin{figure}[h]
  \includegraphics[scale=0.48]{f3.png}
  \caption{Exécution du programme avec et sans optimisation sur la formule $F(a\ U\ (F(b \implies (c\ U\ d))))$}
  \label{fig-3}
\end{figure}
\restoregeometry


\nocite{THESE}
\nocite{PRINCIPLES}
\bibliographystyle{plain-fr}
\bibliography{rapport}

%\end{comment}

\end{document}
