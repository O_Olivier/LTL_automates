open Lexer
open Parser
open Syntax
open Arg
open Unix
open Printf
open Automata
open Automata2dot

(* Alias *)
let parse = Parser.formule Lexer.read

(* Analyse depuis un string, à utiliser pour du debug *)
let analyse_from_string s =
  let lexbuf = Lexing.from_string s in
  parse lexbuf

(* Analyse une formule stockée dans un fichier *)
and analyse_from_filename f_name =
  let f_in = open_in f_name in
  let lexbuf = Lexing.from_channel f_in in
  let p = parse lexbuf in
  close_in f_in;
  p

                
(***********************************)
(* Manage files passed as argument *)
let test_files:string list ref = ref []
    
let add_file_name f_name =
  test_files := f_name :: !test_files

let is_file f_name =
  match (stat f_name).st_kind with
    S_REG -> true
  | _ -> false
(***********************************)

(* Add the content of tests/ directory
 * to the files to analyse *)
let add_test_directory () =
  let open Printf in
  let dir_name =
    sprintf "%s/tests/" (getcwd ()) in
  let dh : dir_handle = opendir dir_name in
  let rec aux () =
    try let f_name = readdir dh in
        if Filename.check_suffix f_name ".formule" then
          add_file_name ("tests/"^f_name);
        aux ()
    with End_of_file -> ()
  in
  aux ();
  (* Filter to get only regular files,
   * that is no directory *)
  test_files :=
    List.filter is_file !test_files;
  closedir dh

(* Booleens pour les options d'execution *)
let no_aut = ref false
let debug = ref false
let min_debug = ref false
let min_display = ref false
let no_opt = ref false
              
(* Parse command-line arguments cf module Arg *)
let parse_arg () =
  let speclist =
    [
      ("-all", Unit add_test_directory,
       "Pass all test files as argument");
      ("-no_aut", Unit (fun () -> no_aut := true),
       "No automata construction");
      ("-min_display", Unit (fun () -> min_display := true),
       "Display minimum informations");
      ("-debug", Unit (fun () -> debug := true),
       "Debug mode");
      ("-min_debug", Unit (fun () -> min_debug := true),
       "Debug mode, minimum");
      ("-no_opt", Unit (fun () -> no_opt := true),
       "No optimisations")
    ]
  and anon_fun = function s -> add_file_name s
  and usage_msg = "Usage : exec -all [-no_aut|-min_display|-debug] "
                  ^"| [-all|-no_aut|-min_display|-debug] file1 [file2 ..]"
  in
  Arg.parse speclist anon_fun usage_msg


(* Print sub formulas *)
let print_sub_formulas (sf:formule list) =
  print_string "Sous-formules : ";
  print_list_formulas sf;
  print_newline ()

(* Print states *)
let print_states (st_list:state list) (msg:string) =
  printf "%s [Nbr:%d] : \n" msg (List.length st_list);
  if not !min_display then
    List.iter
      (fun (fl,nom) -> printf "%s : " nom;
                       print_list_formulas fl;
                       print_newline ())
      st_list

(* Print state names *)
let print_state_names (st_list:state list) (msg:string) =
  printf "%s [Nbr:%d] : \n" msg (List.length st_list);
  if not !min_display then
    begin
      let l = List.map (fun (g,nom) -> nom) st_list in
      print_list l print_string;
      print_newline ()
    end

(* Print transitions *)
let print_transitions (tr:transitions) =
  printf "Transitions [Nbr:%d] :\n" (List.length tr);
  if not !min_display then
    List.iter
      (fun ((_,nom1),sigma,(_,nom2)) ->
        printf "%s -> " nom1;
        print_list_formulas sigma;
        printf " -> %s\n" nom2)
      (List.rev tr)

(* Variables pour afficher des informations 
 * sur les optimisations *)
let nb_etats_avant_opt = ref 0
let nb_etats_apres_opt = ref 0
let taille_formule_avant_opt = ref 0
let taille_formule_apres_opt = ref 0


(* Main function :
 * - parse les arguments
 * - pour chaque fichier en argument
 *   - analyse son contenu
 *   - affiche la formule obtenue
 *   - affiche la formule en nnf
 *   - affiche la formule optimisée
 *   - fait des trucs (en fonction des options passées en argument)
 *)
let handle f_name =
  (* Formule parsée *)
  let f = analyse_from_filename f_name in
  
  (* Formule sous nnf *)
  let f_nnf = forme_normale_neg f in
  printf "Formule : %s [taille : %d]\n"
         (formule_to_string f_nnf)
         (taille f_nnf);
  taille_formule_avant_opt := !taille_formule_avant_opt + taille f_nnf;


  (* Formule après optimisation *)
  let f_opt =
    if !no_opt then f_nnf
    else optimise_formule f_nnf
  in
  taille_formule_apres_opt := !taille_formule_apres_opt
                              + taille f_opt;

  (* Construit les sous-formules et les états *)
  let sf = sous_formules f_opt in
  let l = construit_parties_et_neg sf in
  let l1 = met_sous_nnf_supprime_doublons l in
  let l2 = List.map (fun fl -> supprime_doublons (True::fl)) l1 in
  let l3 =
    List.filter
      (fun q ->
        coherent q && maximal sf q && conforme_semantique sf q)
      l2
  in
  nb_etats_avant_opt := !nb_etats_avant_opt + (List.length l3);
  let l4 =
    if !no_opt then l3
    else optimisation_G f_opt l3
  in
  let etats = List.mapi
                (fun i q -> let nom = Printf.sprintf "q%d" i
                            in (q,nom))
                l4 in
  let tr = construit_transitions sf etats in
  let etats_init = etats_initiaux f_opt etats in
  let etats_accessibles =
    if !no_opt then etats
    else accessibles etats_init etats tr
  in
  let etats_acceptant =
    etats_acceptant sf f_opt etats_accessibles in
  let etats_acceptant_flat = List.flatten etats_acceptant in
  
  let tr_accessibles = filtre_tr_accessibles etats_accessibles tr in
  let alphabet = ["a";"b";"c";"d";"e"] in
  let aut = (etats_accessibles,alphabet,tr_accessibles,
             etats_init,etats_acceptant_flat)
  in
  nb_etats_apres_opt := !nb_etats_apres_opt
                        + List.length etats_accessibles;


  if !debug then
    begin
      printf "Formule sous nnf : %s [taille : %d]\n"
             (formule_to_string f_nnf)
             (taille f_nnf);
      printf "Formule après optimisations : %s [taille : %d]\n"
             (formule_to_string f_opt)
             (taille f_opt);
      print_sub_formulas sf
    end;
    
  if !min_debug then
    begin      
      print_string "Nombres d'états au cours de la réduction :\n";
      printf "* Construction des parties et négations : %d\n"
               (List.length l);
      printf "* Respectent coherence, maximalite et semantique : %d\n"
             (List.length l3);
      printf "* Optimisation pour G operateur principal : %d\n"
             (List.length l4);
      printf "* Suppression des états inaccessibles : %d\n"
             (List.length etats_accessibles)
    end;
  
  
  if !debug then
    begin
      print_states etats_accessibles "Etats";
      print_state_names etats_init "Etats initiaux";
      printf "Etats acceptant [Nbr:%d] :\n"
                 (List.length etats_acceptant_flat);
      if not !min_display then
        begin
          print_list etats_acceptant
                     (fun st_list -> print_list st_list
                                                (fun (q,nom) -> print_string nom));
          print_newline ()
        end;
      print_transitions tr_accessibles;
      if !no_aut then print_newline ()
    end;
     
  if not !no_aut then
    begin
      let f_name_graph =
        (Filename.remove_extension f_name)^"_graph" in
      printf "[Constuction de l'automate dans %s.dot]\n\n"
             f_name_graph;
      pr_automate aut f_name_graph
    end

let () =
  let debut = Sys.time () in
  parse_arg ();
  (* Iterate over the filenames provided *)  
  List.iter
    (fun f_name ->
      try
        handle f_name
      with
      | SyntaxError msg ->
         printf "[%s] -- SyntaxError: %s\n" f_name msg
      | Parser.Error ->
         printf "[%s] -- Parser.Error" f_name
    )                      
    !test_files;
  let fin = Sys.time () in
  let temps_exec = fin -. debut in
  print_string "\n[Statistiques d'execution]\n";
  printf "Nombre de fichiers traités : %d\n"
         (List.length !test_files);
  print_string "Pourcentage de réduction du nombre d'états : ";
  printf "%3.2f %%\n"
         (100. *. (float (!nb_etats_avant_opt - !nb_etats_apres_opt))
          /. (float !nb_etats_avant_opt));
  print_string "Pourcentage de réduction de la taille des formules : ";
  printf "%3.2f %% \n"
         (100. *. (float (!taille_formule_avant_opt - !taille_formule_apres_opt))
          /. (float !taille_formule_avant_opt));
  printf "Temps d'execution : %f s\n" temps_exec
