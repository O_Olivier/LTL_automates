{
open Lexing
open Parser

exception SyntaxError of string
}

let var_prop = ['a'-'z']* '\''*

let space = [' ' '\t' '\n' '\r']+ | "\r\n"

let and_symb = ['A''a']['N''n']['D''d'] | "&&" | "&"
let or_symb  = ['O''o']['R''r'] | "||" | "|"
let implies  = ['I''i']"mplies" | "IMPLIES" | "=>" | "->"
let next     = ['N''n']"ext" | "NEXT" | "X"
let until    = ['U''u']"ntil" | "UNTIL" | "U"
let eventually = "F"
let always   = "G"
let weak_until = "W"
let since = "S" | "Since"
let weak_since = "WS" | "WeakSince"
let previous = "Y" | "Prev" | "P"

rule read =
  parse
  | space         { read lexbuf }
  | "("		  { L_PAR }
  | ")"		  { R_PAR }
  | "true" 	  { TRUE }
  | "false"	  { FALSE }
  | "not"	  { NOT }
  | and_symb	  { AND }
  | or_symb	  { OR }
  | implies	  { IMPLIES }
  | next	  { NEXT }
  | until	  { UNTIL }
  | eventually	  { EVENTUALLY }
  | always	  { ALWAYS }
  | weak_until	  { WEAKUNTIL }
  | since	  { SINCE }
  | weak_since	  { WEAKSINCE }
  | previous	  { PREV }
  | var_prop	  { STRING (Lexing.lexeme lexbuf) }
  | eof		  { EOF }
  | _		  { raise (SyntaxError ("Unexpected char: "
    	       	     		      ^(Lexing.lexeme lexbuf))) }